package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double n,l=1, k=-1,i=2,r=1;

        System.out.println("Input number of stage");
        n=scanner.nextDouble();

        while (i<=n)
        {
            l=l+k*(1/i);
            k=-k;
            r=r+(1/i);
            i++;
        }
        System.out.println("After "+n+" stage he stopped at a distance: "+l+" km");
        System.out.println("He passed: "+r+" km");
    }
}
